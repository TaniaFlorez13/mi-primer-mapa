function initMap(){

    var mymap=L.map('visor', {center:[4.625770, -74.172777], zoom:11});
     
    /*L.tileLayer('https://api.tiles.mapbox.com/v4{id}/{z}/{x}/{y},png?access_token=pk.eyJ1ljoibWFwYm94liwiYSl6lmNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214ArilSLbB6B5aw',{
        maxZoom:20,
        id:'mapbox.streets'
    }).addTo(mymap);*/
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'sk.eyJ1IjoidGFuaWFmbG9yZXoiLCJhIjoiY2tjd2NoOXhtMDdrODJ6c3p3cDBjMzZ0eCJ9.R63m1IU2JHjkycmoROR1hg'
}).addTo(mymap);

//Marcador Parque Salitre
var marker = L.marker([4.66805556, -74.0913889]).addTo(mymap);
marker.bindPopup("<b>Parque Salitre Mágico</b><br>¡Diversión extrema!.").openPopup();


//Marcador Paque Simon Bolivar
var marker = L.marker([4.658055556, -74.09388889]).addTo(mymap);
marker.bindPopup("<b>Parque Simón Bolivar</b>").openPopup();


    
//Marcador Parque Tercer Milenio
var marker = L.marker([4.5975, -74.08166667]).addTo(mymap);
marker.bindPopup("<b>Parque Tercer Milenio</b>").openPopup();


//Marcador de la panaderia más reconocida del barrio
var marker = L.marker([4.676116667, -74.04757778]).addTo(mymap);
marker.bindPopup("<b>Juan Valdéz Café</b>").openPopup();


}   